# Telford Makerspace
# Constitution

*Based on LuneLab Constitution*

## 1) NAME

1. The name of the association shall be Telford Makerspace or Hackspace, hereafter referred
to as the Association.

## 2) AIMS AND OBJECTIVES

1. The aims and objectives of the Association shall be:
 1. to provide shared facilities that enable members to meet, learn, swap skills,
and work on projects either as individuals or collaboratively;
 2. to promote and encourage craft, technical, scientific and artistic skills
through social collaboration and education.
 3. to promote the Hacker Ethos

## 3) EQUALITY STATEMENT

1. The Association is fully committed to the principles of the equality of
opportunity. No participant, volunteer, job applicant or employee of the
Association will receive less favourable treatment on the grounds of gender,
age, colour, disability, ethnic origin, parental or marital status, religious belief,
social class or sexual preference than any other;
 2. The Association will regard discrimination by any employee, participant or
volunteer as grounds for disciplinary action under the relevant Association rules.

## 4) POWERS
1. To further its objectives the Association may do all such lawful things as may
further the Association’s objectives and, in particular, the Association has power:
 1. to raise funds, and to open a bank account to manage such funds;
 2. to buy, take on lease or in exchange, hire or otherwise acquire any property
and to maintain and equip it for use;
 3. to sell, lease or otherwise dispose of all or any part of the property belonging
to the Association;
 4. to enter into any partnership or joint venture arrangement with any other
association;
 5. to publicise and promote the work of the Association and organise meetings,
training courses, events or seminars;
 6. to employ staff, engage consultants, and take on volunteers as necessary.

## 5) MEMBERSHIP

1. Membership and access to the facilities owned or run by the Association,
hereafter referred to as the Facilities, shall be open to all on payment of
relevant fees (if applicable);
2. The Association may determine categories of membership, assign members to
a category and determine the benefits of membership between such categories
3. All members will be subject to the regulations of the Constitution and by joining
the Association will be deemed to accept these regulations and codes of practice
that the Association has adopted
4. The Management Committee shall have the power to refuse membership to an
applicant, where it is considered such membership would be detrimental to the
aims, purposes or activities of the Association.
5. Any member of the Association may resign their membership by giving to the
Secretary of the Association written notice to that effect.

## 6) FEES

1. The rate of fees will be managed by the Committee. Members shall be given 2
months advance notice of changes to the fees;
2. The current rates of fees shall be prominently publicised;
3. Fees shall be payable in advance in order to access the Facilities;
4. Any Member who has unpaid fees in excess of 3 months, without discretion from 
the committe, shall cease to be a member;

## 7) MEMBERS RESPONSIBILITIES

1. All members are responsible for their own safety, they should read and
understand the Health and Safety Guidelines, together with the operating
instructions for any equipment and tools they plan to use. Members who identify
any Health and Safety risk, or any act or omission, that could harm members or
bring the Association into disrepute should rectify it and / or report it to a
Committee member immediately;
2. Members should satisfy themselves that they have adequate insurance to cover
themselves for their personal injury and liabilities when undertaking their
activities;
3. Membership of the Association is conditional upon:
 1. providing up-to-date contact details including name, address, phone number
and email address (these will only be available to the Committee for
administrative purposes and will not be provided to anyone else without the
member's consent);
 2. abiding by all of the rules of the Club and the reasonable directions of the
Committee and their organisers, including but not limited to the
Association's Health & Safety policy.

## 8) OFFICERS

1. The Officers of the Association shall be:
 1. Chair;
 2. Secretary;
 3. Treasurer.
2. The Officers shall be elected for a term of 1 year at the Annual General
Meeting.

## 9) COMMITTEE

1. The Association will be managed through the Committee;
2. The Committee shall consist of
 1. the Officers;
 2. a maximum of 6 Members elected at the Annual General Meeting, who shall
serve a term of 1 year.
3. All Committee members shall be Members of the Association;
4. The Committee shall meet at intervals determined by the business of the
Association and a quorum shall be 3 members of the current Committee (all
serving Officers plus all serving Committee Members) on the day the meeting is
held, of whom at least 1 shall be Officers of the Association;
5. Only members of the Committee will have the right to vote at meetings of the
Committee;
6. The Committee will be responsible for adopting new policy, codes of practice
and rules that affect the Association;
7. The Committee shall be empowered to appoint sub-committees and co-opt
members as may be required from time to time for any special purpose or
purposes;
8. The Committee will be responsible for disciplinary hearings of members who
infringe the Association rules/regulations/constitution. The Committee will be
responsible for taking any action of suspension or discipline following such
hearings.

## 10) ELECTIONS

1. Nominations for candidates for the Officers duly proposed and seconded by
individual Members and countersigned by the candidate consenting to serve if
elected, shall be sent to the Chair not less than 28 days prior to the date of the
Annual General Meeting. A list of nominees within each Office shall be available
to the Members at the Annual General Meeting;
2. Retiring Committee members who are willing to serve may stand for
re-election.


## 11) GENERAL MEETINGS

1. Voting:
 1. Every member shall have one vote at General Meetings;
 2. Subject to the provisions of this Constitution a resolution put to the vote at a
General Meeting shall be decided upon by a show of hands, or paper ballot.
Any resolutions must be agreed by a majority of members present. In the
case of equality of votes the Chair of the meeting shall have a second or
casting vote;
 3. The quorum for Meetings will be 25% of membership on the day of the
meeting;
2. General Meetings:
 1. Any member may submit items to the agenda of General Meeting. To
consider any resolution submitted by a member, any such Resolution shall be
sent to the Chair duly proposed and seconded at least 14 days before the date
of the General Meeting;
 2. General Meetings of the Association shall take place on a regular basis, at a
minimum of every 6 months;
 3. The purpose of these meetings is to ensure that Members are given the
opportunity to participate in the decision-making process of the Association,
review the business planning and management processes and to ensure the
Association manages itself in accordance with its Constitution.
3. Annual General Meetings:
 1. The Association shall in each calendar year hold a general meeting of the
Members as its Annual General Meeting (AGM);
 2. The AGM shall take place no later than three months after the end of the
financial year;
 3.  The business of an AGM shall comprise, where appropriate:
  1. Consideration of accounts and balance sheets;
  2. Election of the Committee;
  3. Approval of an annual operating plan;
  4. Approval of a schedule of delegated matters and committee appointments.

3. Calling a General Meeting:
 1. 3 members or 10% of the membership, whichever is the greater, may, in
writing, call a General Meeting.
 2. All General Meetings shall be called giving 28 clear days’ notice to all
Members but may be held at shorter notice if so agreed in writing by a
majority of Members together comprising not less than 70% of the total
membership of the Association.
 3. It shall be the responsibility of the Committee to arrange and publicise a
date, time, a venue and an agenda for each meeting;
 4. The accidental omission to give notice of a meeting to, or non-receipt of
notice of a meeting by any member shall not invalidate proceedings at that
meeting.
4. It shall be the responsibility of the Chair to chair all meetings or a designated
deputy in his/her absence. All meetings must be minuted and shall be publicly
available.

## 12) FINANCE

1. The financial year of the Association will run from 6th April to 5th April;
2. Any money acquired by the Association, including donations, contributions and
bequests, shall be paid into an account operated by the Committee in the name
of the Association. All funds must be applied to the objects of the Association
and for no other purpose;
3. Bank accounts shall be opened in the name of the Association;
4. Any income/expenditure shall be the responsibility of the Treasurer who will be
accountable to ensure funds are utilised effectively and that the Association
stays within budget;
5. Official accounts shall be maintained and will be presented at the Annual
General Meeting. The accounts will be available to all members upon request.


## 13) DISCIPLINE AND APPEALS

1. All complaints regarding the behaviour of members should be submitted in
writing to the Committee;
2. The Committee will meet to hear complaints within 28 days of a complaint
being lodged. The Committee has the power to take appropriate disciplinary
action including the termination of membership if the complaint is upheld;
3. The outcome of a disciplinary hearing will be notified in writing to the person
who lodged the complaint and the Member(s) against whom the complaint was
made within 28 days of the hearing;
4. There will be the right of appeal to the Committee following disciplinary action
being notified. The Committee will consider the appeal within 28 days of the
Committee receiving the appeal. The appeal must be lodged within 7 days of
initial outcome being notified, to the Committee;
5. The Committee or the Member against whom the complaint was made has the
right to have the appeal heard by 3 members of the Association who have not
been involved the complaint in the first stage.

## 14) ALTERATION OF THE CONSTITUTION

1. Amendments to this Constitution must be conveyed to the Secretary formally in
writing. The Committee shall then decide on the date of a General Meeting to
discuss such proposals;
2. This Constitution may only be altered by a majority vote at a General Meeting
and if an amended copy is subsequently circulated to all members within 28 days;

## 15) DISSOLUTION

1. The Association may be dissolved if deemed necessary by the members in a
majority vote at a General Meeting;
2. Once all debts have been paid, any donated assets shall initially be offered back
to their original donators. Any remaining assets or remaining funds will be
transferred to local charities or similar associations at the discretion of the
Committee.
