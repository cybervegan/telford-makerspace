# Duties of Officers

## Secretary
1. Maintenance of the membership list:
 1. Adding new members to the list
 2. Removing members from the list due to resignation
 2. Removing members from the list due to Non-payment of membership fees
 3. Removing members from the list due to Termination of membership resulting from the Discipline and Appeals procedure 
2. Maintenance of the Constitution:
 1. Receipt of proposed amendments to the Consititution
 2. Notifying the committee of the receipt of an amendment proposal so that a General Meeting can be convened to discuss it
 3. Circulation of the proposed amended Constitution to the membership within 28 days of its acceptance by the membership


## Chair
1. Chair all meetings or designate a deputy in their absence
2. Receive and record nominations for candidates for the Officers of the Association
3. Make a "casting vote" in case of a equality of votes
4. Receive and record submissions for the agenda of General Meetings
 


## Treasurer
1. Manage the Association's funds and bank account(s)
2. Ensure the Association's funds are utilised effectively
3. Ensure the Association stays within budget